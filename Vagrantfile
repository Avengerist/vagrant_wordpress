Vagrant.configure("2") do |config|
  config.vm.define "mariadb" do |mariadb|
    mariadb.vm.box = "focal-server-cloudimg-amd64-vagrant.box"
    mariadb.vm.network "public_network",bridge: "wlo1",
      ip: "192.168.88.20",
      netmask: "24"
    mariadb.vm.provision "shell", inline: <<-SHELL  
      sudo apt-get update -y
      sudo apt-get install mariadb-server -y
      ip=`ip a | grep -E -o "192.168..*/" | tr -d "/"`
      sudo sed -i "s/127.0.0.1/$ip/" /etc/mysql/mariadb.conf.d/50-server.cnf
      sudo systemctl restart mariadb
      sudo mysql -u root -e "CREATE DATABASE wordpress_db;"
      sudo mysql -u root -e "CREATE USER 'wp_user'@'%' IDENTIFIED BY '123';"
      sudo mysql -u root -e "GRANT ALL PRIVILEGES ON wordpress_db.* TO 'wp_user'@'%';"
      SHELL
    end
  end

  (11..13).each do |i|
    config.vm.define "wordpress#{i}" do |wordpress|
      wordpress.vm.box = "focal-server-cloudimg-amd64-vagrant.box"
      wordpress.vm.network "public_network",bridge: "wlo1",
        ip: "192.168.88.#{i}",
        netmask: "24"
      wordpress.vm.network "forwarded_port", guest: 80, host: "88#{i}", host_ip: "127.0.0.1"
      wordpress.vm.provision "shell", inline: <<-SHELL  
        sudo apt-get update -y
        sudo apt-get install -y php-curl php-gd php-intl php-mbstring php-soap php-xml php-xmlrpc php-zip php-fpm php-mysql
        sudo apt-get install nginx -y
        sudo systemctl enable --now nginx
        cd /tmp && wget https://wordpress.org/latest.tar.gz
        tar -xzvf latest.tar.gz
        sudo cp -R wordpress /var/www/html
        sudo mkdir /var/www/html/wordpress/wp-content/uploads
        sudo cp /var/www/html/wordpress/wp-config-sample.php /var/www/html/wordpress/wp-config.php
        sudo sed -i 's/database_name_here/wordpress_db/' /var/www/html/wordpress/wp-config.php
        sudo sed -i 's/username_here/wp_user/'  /var/www/html/wordpress/wp-config.php
        sudo sed -i 's/password_here/123/'  /var/www/html/wordpress/wp-config.php
        sudo sed -i 's/localhost/192.168.88.20/'  /var/www/html/wordpress/wp-config.php
        sudo chmod -R 755 /var/www/html/wordpress/
        sudo sed -i "s/index/index index.php/" /etc/nginx/sites-enabled/default
        sudo sed -i "s/^}//" /etc/nginx/sites-enabled/default
        sudo echo   "location ~ \\.php\$ {" >> /etc/nginx/sites-enabled/default
        sudo echo   "          include snippets/fastcgi-php.conf;" >> /etc/nginx/sites-enabled/default 
        sudo echo   "fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;" >> /etc/nginx/sites-enabled/default 
        sudo echo  "}" >> /etc/nginx/sites-enabled/default 
        sudo echo  "}" >> /etc/nginx/sites-enabled/default 
        sudo systemctl reload nginx
        sudo systemctl enable --now php7.4-fpm
        SHELL
      end
    end
  end

  config.vm.define "upstream-balancer" do |balancer|
    balancer.vm.box = "focal-server-cloudimg-amd64-vagrant.box"
    balancer.vm.network "public_network",bridge: "wlo1",
      ip: "192.168.88.200",
      netmask: "24"
    balancer.vm.network "forwarded_port", guest: 8080, host: "8080", host_ip: "127.0.0.1"
    balancer.vm.provision "shell", inline: <<-SHELL  
      sudo apt-get update -y
      sudo apt-get install nginx -y
      sudo systemctl enable --now nginx
      sudo echo "upstream backend {
        server 192.168.88.11;
        server 192.168.88.12;
        server 192.168.88.13;
    }" >> /etc/nginx/conf.d/upstream-balancer.conf
    sudo echo 'server {
        listen 80;
        server_name balancer.local;' >> /etc/nginx/conf.d/upstream-balancer.conf
      
    sudo echo 'location / {
	        proxy_redirect      off;
	        proxy_set_header    X-Real-IP $remote_addr;
	        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
	        proxy_set_header    Host $http_host;
    proxy_pass http://backend;
	}
}' >> /etc/nginx/conf.d/upstream-balancer.conf
      sudo systemctl reload nginx
      SHELL
    end
  end
end
